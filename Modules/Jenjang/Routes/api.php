<?php
Route::group(['prefix'=>'jenjang','middleware'=>['auth:api']],function (){
    Route::post('/create', 'JenjangController@create');
    Route::get('/read', 'JenjangController@read');
    Route::post('/update/{id}', 'JenjangController@update');
    Route::delete('/delete/{id}', 'JenjangController@delete');
});
