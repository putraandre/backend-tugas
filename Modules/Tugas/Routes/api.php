<?php
$prefix = 'Tugas';
Route::group(['prefix'=>strtolower($prefix),'middleware'=>['auth:api']],function () use($prefix){
    Route::post('/create', $prefix.'Controller@create');
    Route::get('/read', $prefix.'Controller@read');
    Route::post('/update/{id}', $prefix.'Controller@update');
    Route::delete('/delete/{id}', $prefix.'Controller@delete');
});
