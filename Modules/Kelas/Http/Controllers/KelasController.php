<?php

namespace Modules\Kelas\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class KelasController extends Controller
{
    private $table = 'kelas';
    public function create(Request $request){
        $data = DB::table($this->table)->insert($request->all());
        if ($data){
            return response()->json('Data Berhasil Disimpan',200);
        }
        return response()->json('Data Gagal Di Simpan',422);
    }

    public function read(Request $request){
        $data = DB::table($this->table)
            ->join('dosen','dosen_id','=',$this->table.'_dosen_id')
            ->join('mata_kuliah','mata_kuliah_id','=',$this->table.'_mata_kuliah_id');
        if ($request->filled('id')){
            return response()->json($data->where($this->table.'_id','=',$request->id)->first());
        }
        return response()->json($data->get());
    }
    public function update(Request $request,$id){
        $data = DB::table($this->table)
            ->where($this->table.'_id','=', $id)
            ->update($request->all());
        if ($data){
            return response()->json('Data Berhasil Disimpan',200);
        }
        return response()->json('Data Gagal Di Simpan',422);
    }
    public function delete($id){
        $data = DB::table($this->table)->where($this->table.'_id','=',$id)->delete();
        if ($data){
            return response()->json('Data Berhasil Dihapus',200);
        }
        return response()->json('Data Gagal Di Hapus',422);
    }
}
