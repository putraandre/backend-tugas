<?php
$prefix = 'KelasMahasiswa';
Route::group(['prefix'=>strtolower($prefix),'middleware'=>['auth:api']],function () use($prefix){
    Route::get('/list', $prefix.'Controller@list');
});
