<?php

namespace Modules\KelasMahasiswa\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class KelasMahasiswaController extends Controller
{
    public function list()
    {
        $data = DB::table('kelas')
            ->leftJoin('kelas_mahasiswa', 'kelas_id', '=', 'kelas_mahasiswa_kelas_id')
            ->leftJoin('dosen', 'dosen_id', '=', 'kelas_dosen_id')
            ->leftJoin('mata_kuliah','mata_kuliah_id','=','kelas_mata_kuliah_id')
            ->join('mahasiswa', 'mahasiswa_id', '=', 'kelas_mahasiswa_mahasiswa_id');
        if (Auth::user()->level==='dosen'){
            $data->where('dosen_id', '=', Auth::id());
        }else{
            $data->where('mahasiswa_id', '=', Auth::id());
        }
        return response()->json($data->get());
    }
}
