<?php

namespace Modules\KumpulTugas\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class KumpulTugasController extends Controller
{
    private $table = 'kumpul_tugas';
    public function create(Request $request){
        $request->request->add([$this->table.'_mahasiswa_id' => Auth::id()]);
        $data = DB::table($this->table)->insert($request->all());
        if ($data){
            return response()->json('Data Berhasil Disimpan',200);
        }
        return response()->json('Data Gagal Di Simpan',422);
    }

    public function read(Request $request){
        $data = DB::table($this->table)
            ->join('tugas','tugas_id','=',$this->table.'_tugas_id')
            ->join('mahasiswa','mahasiswa_id','=',$this->table.'_mahasiswa_id')
        ->where('tugas_id','=',$request->tugas_id);
        if ($request->filled('id')){
            return response()->json($data->where($this->table.'_id','=',$request->id)->first());
        }
        return response()->json($data->get());
    }
    public function update(Request $request,$id){
        $data = DB::table($this->table)
            ->where($this->table.'_id','=', $id)
            ->update($request->all());
        if ($data){
            return response()->json('Data Berhasil Disimpan',200);
        }
        return response()->json('Data Gagal Di Simpan',422);
    }
    public function delete($id){
        $data = DB::table($this->table)->where($this->table.'_id','=',$id)->delete();
        if ($data){
            return response()->json('Data Berhasil Dihapus',200);
        }
        return response()->json('Data Gagal Di Hapus',422);
    }
}
