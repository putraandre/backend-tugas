<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use stdClass;

class AuthController extends Controller
{
    public function login(Request $request){
        $data = new stdClass();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(),422);
        }
        $credentials = $request->only('name', 'password');
        if (Auth::attempt($credentials)) {
            $data->token = $this->token($request);
            $data->users = Auth::user();
            return response()->json($data);
        }
        return $request->all();
    }

    private function token(Request $request)
    {
        $token = Str::random(60);

        $request->user()->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();

        return $token;
    }
}
