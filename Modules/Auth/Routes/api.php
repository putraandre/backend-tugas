<?php

Route::group(['prefix'=>'auth'],function (){
    Route::post('/login', 'AuthController@login');
    Route::post('/update', 'AuthController@update')->middleware('auth:api');
});
