<?php
Route::group(['prefix'=>'jurusan','middleware'=>['auth:api']],function (){
    Route::post('/create', 'JurusanController@create');
    Route::get('/read', 'JurusanController@read');
    Route::post('/update/{id}', 'JurusanController@update');
    Route::delete('/delete/{id}', 'JurusanController@delete');
});
