<?php

namespace Modules\Jurusan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class JurusanController extends Controller
{
    private $table = 'jurusan';
    public function create(Request $request){
        $data = DB::table($this->table)->insert($request->all());
        if ($data){
            return response()->json('Data Berhasil Disimpan',200);
        }
        return response()->json('Data Gagal Di Simpan',422);
    }

    public function read(Request $request){
        $data = DB::table($this->table)
            ->join('jenjang','jenjang_id','=',$this->table.'_jenjang_id');
        if ($request->filled('id')){
            return response()->json($data->where($this->table.'_id','=',$request->id)->first());
        }
        return response()->json($data->get());
    }
    public function update(Request $request,$id){
        $data = DB::table($this->table)
            ->where($this->table.'_id','=', $id)
            ->update($request->all());
        if ($data){
            return response()->json('Data Berhasil Disimpan',200);
        }
        return response()->json('Data Gagal Di Simpan',422);
    }
    public function delete($id){
        $data = DB::table($this->table)->where($this->table.'_id','=',$id)->delete();
        if ($data){
            return response()->json('Data Berhasil Dihapus',200);
        }
        return response()->json('Data Gagal Di Hapus',422);
    }
}
