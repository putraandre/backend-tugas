@extends('layouts')
@section('title','Kelas Saya')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="table" width="100%" cellspacing="0" url="/api/kelasmahasiswa/list"></table>
            </div>
        </div>
    </div>
@endsection
@section('after_init')
    <script>
        const columns = [
            {
                "title": "No",
                "data": null,
                "name": null
            },
            {
                "title": "Nama Kelas",
                "data": 'kelas_nama',
                "name": 'kelas_nama'
            },
            {
                "title": "Dosen",
                "data": 'dosen_nama',
                "name": 'dosen_nama'
            },
            {
                "title": "Mata kuliah",
                "data": 'mata_kuliah_nama',
                "name": 'mata_kuliah_nama'
            },
            {
                "title": "Aksi",
                "data": 'kelas_id',
                "name": 'kelas_id'
            },
        ];
        cd = [
            {
                targets: 0,
                title: 'No.',
                orderable: false,
                visible: true,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                },
            },
            {
                targets: -1,
                title: 'Aksi',
                orderable: false,
                visible: true,
                render: function (data, type, full, meta) {
                    return `<a href="/mahasiswa/tugas/${data}" class="btn btn-info" title="Lihat"><i class="fa fa-eye"></i></a>`;
                },
            }
        ]
    </script>
@endsection
