@extends('layouts')
@section('title','Kelas Saya')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="table" width="100%" cellspacing="0"
                       url="/api/kelasmahasiswa/list"></table>
            </div>
        </div>
    </div>
    <div class="modal  fade" id="lihat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Daftar Tugas</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Soal</th>
                            <th>Batas Pengumpulan</th>
                            <th>file</th>
                            <th>Nilai</th>
                            <th>Upload</th>
                        </tr>
                        </thead>
                        <tbody id="tugas"></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after_init')
    <script>
        $('#lihat').on('hidden.bs.modal', function (e) {
            $('#tugas').children().remove();
        });
        function lihat(id) {
            $.ajax(`/api/tugas/read?kelas_id=${id}`,
                {headers:{'Authorization':`Bearer ${token}`}})
                .done(e=>{
                    let i = 1;
                    $.each(e,(k,v)=>{
                        $('#tugas').append(`<tr>
                            <td>${i++}</td>
                            <td>${v.tugas_soal}</td>
                            <td>${v.tugas_waktu}</td>
                            <td>${v.kumpul_tugas_upload}</td>
                            <td>${v.kumpul_tugas_nilai}</td>
                            <td>${v.tugas_id}</td>
                    </tr>`)
                    })
                })
                .then($('#lihat').modal('show'))
                .catch(()=>{

            })
        }

        const columns = [
            {
                "title": "No",
                "data": null,
                "name": null
            },
            {
                "title": "Nama Kelas",
                "data": 'kelas_nama',
                "name": 'kelas_nama'
            },
            {
                "title": "Dosen",
                "data": 'dosen_nama',
                "name": 'dosen_nama'
            },
            {
                "title": "Mata kuliah",
                "data": 'mata_kuliah_nama',
                "name": 'mata_kuliah_nama'
            },
            {
                "title": "Aksi",
                "data": 'kelas_id',
                "name": 'kelas_id'
            },
        ];
        cd = [
            {
                targets: 0,
                title: 'No.',
                orderable: false,
                visible: true,
                render: (data, type, full, meta) => meta.row + 1
            },
            {
                targets: -1,
                title: 'Aksi',
                orderable: false,
                visible: true,
                render: (data, type, full, meta) => {
                    return `<button onclick='lihat(${data})' class="btn btn-info" title="Lihat"><i class="fa fa-eye"></i></button>`
                }

            },];
    </script>
@endsection
