<script src="/assets/vendor/jquery/jquery.min.js"></script>
<script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="/assets/js/sb-admin-2.min.js"></script>
{{--<script src="/assets/vendor/chart.js/Chart.min.js"></script>--}}
{{--<script src="/assets/js/demo/chart-area-demo.js"></script>--}}
{{--<script src="/assets/js/demo/chart-pie-demo.js"></script>--}}
<script src="/assets/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    const token = localStorage.getItem('token');
    //table
    let cd = [
        {
            targets: 0,
            title: 'No.',
            orderable: false,
            visible: true,
            render: function (data, type, full, meta) {
                return meta.row + 1;
            },
        },
        {
            targets: -1,
            title: 'Aksi',
            orderable: false,
            visible: true,
            render: function (data, type, full, meta) {
                return `<button onclick="ubah('${table}',${data})" class="btn btn-info" title="Edit"><i class="fa fa-edit"></i></button>
            <button onclick="hapus('${table}',${data})" class="btn btn-danger" title="Delete"><i class="fa fa-trash"></i></button>`;
            },
        }
    ];

</script>
@yield('after_init')
<script>
    $(function () {
        //auth function
        let current = window.location.pathname.split('/')[1];
        let users = JSON.parse(localStorage.getItem('users'));
        if (!users){
            return window.location.href = `/login`
        }
        if (users.level!==current){
            return window.location.href = `/${users.level}/kelas`
        }


        //datatable function
        $('#table').DataTable({
            dom: "Bfrtip",
            responsive: true,
            language: {
                "emptyTable": "Data Kosong"
            },
            fixedHeader: true,
            keys: true,
            ajax: {
                url: `${$('#table').attr('url')}`,
                'type': 'GET',
                'beforeSend': function (request) {
                    request.setRequestHeader("Authorization", `Bearer ${token}`);
                },
                "dataSrc": "",
            },
            dataType: "json",
            stateSave: true,
            pagingType: "full_numbers",
            pageLength: 8,
            lengthMenu: [[5, 8, 15, 20], [5, 8, 15, 20]],
            autoWidth: true,
            orderable: true,
            columns: columns,
            columnDefs: cd,
        })
    })
</script>
