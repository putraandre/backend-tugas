<!DOCTYPE html>
<html lang="en">
<head>
    @include('layout.css')
</head>
<body id="page-top">
<div id="wrapper">
    @if(str_contains(URL::current(), 'mahasiswa'))
        @include('layout.mahasiswaMenu')
    @else
        @include('layout.dosenMenu')
    @endif
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('layout.topbar')
            <div class="container-fluid">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">@yield('title','Default')</h1>
                </div>
                @yield('content','Empty Content')
            </div>
        </div>
        @include('layout.footer')
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@include('layout.js')
</body>
</html>
