<?php

use Illuminate\Support\Facades\Route;

Route::get('/',function (){
    return view('login');
});
Route::group(['middleware'=>'guest'],function(){
    Route::get('/login',function (){
        return view('login');
    })->name('login');
    Route::get('/register',function (){
        return view('register');
    })->name('register');
});

Route::group(['prefix'=>'/dosen'],function (){
    Route::get('/kelas',function (){
        return view('dosen.kelas');
    })->name('dosen.kelas');
});

Route::group(['prefix'=>'/mahasiswa'],function (){
    Route::get('/kelas',function (){
        return view('mahasiswa.kelas');
    })->name('mahasiswa.kelas');
});
