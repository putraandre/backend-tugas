<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KelasSeeder extends Seeder
{
    function r($param){
        $data =  DB::table($param)->inRandomOrder()->first();
        $id = $param.'_id';
        return $data->$id;
    }
    public function run()
    {
        $data = DB::table('dosen')
            ->get();
        $index = 1;
        foreach ($data as $v) {
            DB::table('kelas')->insert([
                'kelas_dosen_id'=>$v->dosen_id,
                'kelas_mata_kuliah_id'=>$this->r('mata_kuliah'),
                'kelas_nama'=>'kelas_nama'.$index,
            ]);
            $index++;
        }
    }
}
