<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TugasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::table('kelas')->get();
        $index = 1;
        $date = Carbon::yesterday();
        foreach ($data as $k){
            DB::table('tugas')
                ->insert([
                    'tugas_kelas_id'=>$k->kelas_id,
                    'tugas_soal'=>'soal'.$index,
                    'tugas_waktu'=>$date,
                ]);
            $index++;
        }
    }
}
