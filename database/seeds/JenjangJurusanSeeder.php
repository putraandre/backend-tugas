<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenjangJurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenjang')
            ->insert([
                [
                    'jenjang_nama' => 'D3'
                ],
                [
                    'jenjang_nama' => 'S1'
                ],
            ]);
        DB::table('jurusan')
            ->insert([
                [
                    'jurusan_jenjang_id' => 1,
                    'jurusan_nama'=>'Komputer Akuntansi'

                ],
                [
                    'jurusan_jenjang_id' => 1,
                    'jurusan_nama'=>'Teknik Komputer'
                ],
                [
                    'jurusan_jenjang_id' => 1,
                    'jurusan_nama'=>'Manajemen Informatika '
                ],
                [
                    'jurusan_jenjang_id' => 2,
                    'jurusan_nama'=>'Sistem Informasi'
                ],
                [
                    'jurusan_jenjang_id' => 2,
                    'jurusan_nama'=>'Teknik Informatika '
                ],
            ]);
    }
}
