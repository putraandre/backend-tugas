<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $f = Faker::create('id_ID');
        DB::table('users')->insert([
            'name'=>'dosen',
            'password'=>Hash::make('password'),
            'level'=>'dosen'
        ]);
        for ($i=1;$i<=49;$i++){
            DB::table('users')->insert([
                'name'=>$f->userName,
                'password'=>Hash::make('password'),
                'level'=>'dosen'
            ]);
        }
        DB::table('users')->insert([
            'name'=>'mahasiswa',
            'password'=>Hash::make('password'),
        ]);
        for ($i=1;$i<=99;$i++){
            DB::table('users')->insert([
                'name'=>$f->userName,
                'password'=>Hash::make('password')
            ]);
        }
    }
}
