<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class DosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $f = Faker::create('id_ID');
        $data = DB::table('users')
            ->where('level','=','dosen')
            ->get();
        $index = 1;
        foreach ($data as $v) {
            DB::table('dosen')->insert([
                'dosen_id'=>$v->id,
                'dosen_nip'=>11111111111111111+$index,
                'dosen_nama'=>$f->name
            ]);
            $index++;
        }
    }
}
