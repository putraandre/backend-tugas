<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    function randj(){
        return DB::table('jurusan')->inRandomOrder()->first()->jurusan_id;
    }
    public function run()
    {
        $f = Faker::create('id_ID');
        $data = DB::table('users')
            ->where('level','=','mahasiswa')
            ->get();
        $index = 1;
        foreach ($data as $v) {
            DB::table('mahasiswa')->insert([
                'mahasiswa_id'=>$v->id,
                'mahasiswa_jurusan_id'=>$this->randj(),
                'mahasiswa_nim'=>11111111+$index,
                'mahasiswa_nama'=>$f->name,
            ]);
            $index++;
        }
    }
}
