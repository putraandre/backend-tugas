<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MataKuliahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $index = 1;
        for ($i=1;$i<=59;$i++){
            DB::table('mata_kuliah')
                ->insert([
                    'mata_kuliah_jurusan_id'=>strlen($i)!==1 ?  substr($i, 0, 1) : 1,
                    'mata_kuliah_nama'=>'mata_kuliah'.$index,
                ]);
            $index++;
        }
    }
}
