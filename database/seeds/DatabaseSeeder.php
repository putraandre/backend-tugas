<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             JenjangJurusanSeeder::class,
             UserSeeder::class,
             DosenSeeder::class,
             MahasiswaSeeder::class,
             MataKuliahSeeder::class,
             KelasSeeder::class,
             KelasMahasiswaSeeder::class,
             TugasSeeder::class,
         ]);
    }
}
