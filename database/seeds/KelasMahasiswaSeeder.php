<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KelasMahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kelas = DB::table('kelas')
            ->join('mata_kuliah','mata_kuliah_id','=','kelas_mata_kuliah_id')
            ->get();
        $mahasiswa = DB::table('mahasiswa')->get();
        foreach ($kelas as $k){
            foreach ($mahasiswa as $m){
                if ($k->mata_kuliah_jurusan_id === $m->mahasiswa_jurusan_id)
                DB::table('kelas_mahasiswa')->insert([
                    'kelas_mahasiswa_kelas_id'=>$k->kelas_id,
                    'kelas_mahasiswa_mahasiswa_id'=>$m->mahasiswa_id,
                ]);
            }
        }
    }
}
