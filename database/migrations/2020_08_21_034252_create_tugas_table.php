<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tugas', function (Blueprint $table) {
            $table->bigIncrements('tugas_id');
            $table->unsignedBigInteger('tugas_kelas_id');
            $table->foreign('tugas_kelas_id')->references('kelas_id')->on('kelas')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->text('tugas_soal')->nullable();
            $table->dateTime('tugas_waktu')->nullable();
            $table->timestamp('tugas_created_at')->useCurrent();
            $table->timestamp('tugas_updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tugas');
    }
}
