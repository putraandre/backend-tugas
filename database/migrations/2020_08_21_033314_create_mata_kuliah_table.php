<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMataKuliahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mata_kuliah', function (Blueprint $table) {
            $table->bigIncrements('mata_kuliah_id');
            $table->unsignedBigInteger('mata_kuliah_jurusan_id');
            $table->foreign('mata_kuliah_jurusan_id')->references('jurusan_id')->on('jurusan')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('mata_kuliah_nama');
            $table->timestamp('mata_kuliah_created_at')->useCurrent();
            $table->timestamp('mata_kuliah_updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mata_kuliah');
    }
}
