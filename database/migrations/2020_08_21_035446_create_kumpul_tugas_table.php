<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateKumpulTugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kumpul_tugas', function (Blueprint $table) {
            $table->bigIncrements('kumpul_tugas_id');
            $table->unsignedBigInteger('kumpul_tugas_tugas_id');
            $table->foreign('kumpul_tugas_tugas_id')->references('tugas_id')->on('tugas')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unsignedBigInteger('kumpul_tugas_mahasiswa_id');
            $table->foreign('kumpul_tugas_mahasiswa_id')->references('mahasiswa_id')->on('mahasiswa')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->longText('kumpul_tugas_upload');
            $table->unsignedBigInteger('kumpul_tugas_nilai')->default(0);
            $table->timestamp('kumpul_tugas_created_at')->useCurrent();
            $table->timestamp('kumpul_tugas_updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kumpul_tugas');
    }
}
