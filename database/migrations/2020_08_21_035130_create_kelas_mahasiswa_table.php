<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKelasMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelas_mahasiswa', function (Blueprint $table) {
            $table->unsignedBigInteger('kelas_mahasiswa_kelas_id');
            $table->unsignedBigInteger('kelas_mahasiswa_mahasiswa_id');
            $table->foreign('kelas_mahasiswa_mahasiswa_id')->references('mahasiswa_id')->on('mahasiswa')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('kelas_mahasiswa_kelas_id')->references('kelas_id')->on('kelas')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelas_mahasiswa');
    }
}
