<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateJurusanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurusan', function (Blueprint $table) {
            $table->bigIncrements('jurusan_id');
            $table->unsignedBigInteger('jurusan_jenjang_id');
            $table->foreign('jurusan_jenjang_id')->references('jenjang_id')->on('jenjang')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('jurusan_nama');
            $table->timestamp('jurusan_created_at')->useCurrent();
            $table->timestamp('jurusan_updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurusan');
    }
}
