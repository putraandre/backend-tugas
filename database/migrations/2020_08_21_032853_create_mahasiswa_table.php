<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa', function (Blueprint $table) {
            $table->unsignedBigInteger('mahasiswa_id');
            $table->foreign('mahasiswa_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unsignedBigInteger('mahasiswa_jurusan_id');
            $table->foreign('mahasiswa_jurusan_id')->references('jurusan_id')->on('jurusan')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('mahasiswa_nim')->unique();
            $table->string('mahasiswa_nama');
            $table->timestamp('mahasiswa_created_at')->useCurrent();
            $table->timestamp('mahasiswa_updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa');
    }
}
