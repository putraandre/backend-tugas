<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelas', function (Blueprint $table) {
            $table->bigIncrements('kelas_id');
            $table->unsignedBigInteger('kelas_mata_kuliah_id');
            $table->foreign('kelas_mata_kuliah_id')->references('mata_kuliah_id')->on('mata_kuliah')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unsignedBigInteger('kelas_dosen_id');
            $table->foreign('kelas_dosen_id')->references('dosen_id')->on('dosen')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('kelas_nama');
            $table->timestamp('kelas_created_at')->useCurrent();
            $table->timestamp('kelas_updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelas');
    }
}
